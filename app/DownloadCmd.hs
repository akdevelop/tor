module DownloadCmd
( run
) where

import           Control.Monad
import qualified Data.Set      as Set
import qualified Meta
import qualified Titles
import qualified Torrents
import qualified YTS

instance Torrents.Movie YTS.Movie where
  title = YTS.title
  url = YTS.url

run :: String -> String -> String -> IO ()
run titlesFile descrFile imagesDir = do
    titles <- (Set.fromList . clean . Titles.fromString) <$> readFile titlesFile
    movies <- (recent . missing titles) <$> YTS.list
    unless (null movies) $ do
      let shortList = take 10 movies
      Torrents.download shortList
      Meta.addToMeta descrFile imagesDir (map YTS.uid shortList)
  where clean = map Titles.sanitize
        recent = filter $ (>= 2013) . YTS.year
        missing ts = filter $ (`Set.notMember` ts) . Titles.sanitize . YTS.title
