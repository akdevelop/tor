import qualified DownloadCmd
import qualified MetaCmd
import           Options.Applicative

type TitlesFile = String
type DescrFile = String
type ImagesDir = String

data Command
  = DownloadOpts TitlesFile DescrFile ImagesDir
  | MetaOpts TitlesFile DescrFile ImagesDir

data Options = Options Command

withInfo :: Parser a -> String -> ParserInfo a
withInfo opts desc = info (helper <*> opts) $ progDesc desc

parseDownloadOpts :: Parser Command
parseDownloadOpts = DownloadOpts
  <$> strOption ( short 't'
    <> long "titles" <> metavar "FILE" <> value "movies.txt"
    <> help "File with existing titles")
  <*> strOption ( short 'd'
    <> long "details" <> metavar "FILE" <> value "movies.json"
    <> help "File with movie descriptions")
  <*> strOption ( short 'i'
     <> long "images" <> metavar "DIRECTORY" <> value "movies.img"
     <> help "Directory with movie images")

parseMetaOpts :: Parser Command
parseMetaOpts = MetaOpts
  <$> strOption ( short 't'
    <> long "titles" <> metavar "FILE" <> value "movies.txt"
    <> help "File with existing titles")
  <*> strOption ( short 'd'
    <> long "details" <> metavar "FILE" <> value "movies.json"
    <> help "File with movie descriptions")
  <*> strOption ( short 'i'
     <> long "images" <> metavar "DIRECTORY" <> value "movies.img"
     <> help "Directory with movie images")

parseCommand :: Parser Command
parseCommand = subparser $
  command "download" (parseDownloadOpts `withInfo` "Download new torrent movies") <>
  command "meta" (parseMetaOpts `withInfo` "Retrieve movie descriptions")

parseOptions :: Parser Options
parseOptions = Options <$> parseCommand

main :: IO ()
main = run =<< execParser
    (parseOptions `withInfo` "Torrent automation utilities")

run :: Options -> IO ()
run (Options (DownloadOpts titlesFile descrFile imagesDir)) =
  DownloadCmd.run titlesFile descrFile imagesDir

run (Options (MetaOpts titlesFile descrFile imagesDir)) =
  MetaCmd.run titlesFile descrFile imagesDir
