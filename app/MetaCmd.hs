module MetaCmd
( run
) where

import           Control.Monad
import qualified Data.ByteString.Lazy as B
import qualified Data.Set             as Set
import qualified Meta
import           System.IO
import           Text.Printf
import qualified Titles

titlesWithoutMeta :: [String] -> [Meta.Movie] -> [String]
titlesWithoutMeta titles movies =
  let dts = Set.fromList $ map (Titles.sanitize . Meta.title) movies
  in filter ((`Set.notMember` dts) . Titles.sanitize) titles

run :: String -> String -> String -> IO ()
run titlesFile descrFile imagesDir = do
    titles <- Titles.fromString <$> readFile titlesFile
    movieList <- Meta.parse <$> B.readFile descrFile
    uids <- foldM (\uids title -> do
        printf "%-27.25s" title >> hFlush stdout
        maybeUid <- Meta.fetchMovieIDByTitle title
        uid <- case maybeUid of
                 Just u -> do
                   putStrLn $ "[" ++ show u ++ "]"
                   return u
                 Nothing -> askForID
        return $ uid : uids
      ) [] (take 10 (titlesWithoutMeta titles movieList))
    Meta.addToMeta descrFile imagesDir uids
  where askForID = read . head . lines <$> (putStr "id=? " >> hFlush stdout >> getLine)
