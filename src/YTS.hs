{-# LANGUAGE OverloadedStrings #-}

module YTS
( Movie(..)
, list
) where

import           Control.Lens
import           Control.Monad
import           Data.Aeson.Lens
import           Data.Aeson.Types     (Value)
import           Data.ByteString.Lazy (ByteString)
import           Data.Maybe
import qualified Data.Text            as T
import           Network.Wreq

data Movie = Movie
  { uid   :: Integer
  , title :: String
  , year  :: Integer
  , url   :: String
  } deriving (Show)

makeMovie :: Value -> Maybe Movie
makeMovie m = do
    mId <- m ^? key "id" . _Integer
    mTitle <- m ^? key "title_long" . _String
    mYear <- m ^? key "year" . _Integer
    torrent <- m ^.. key "torrents"
                   . _Array
                   . folded
                   . filtered only1080
                  ^? _head
    mUrl <- torrent ^? key "url" . _String
    if hasHorror m
      then Nothing
      else return Movie
        { uid = mId
        , title = T.unpack mTitle
        , year = mYear
        , url = T.unpack mUrl }
  where only1080 = (== "1080p") . (^. key "quality" . _String)
        isHorror = (== "Horror") . (^. _String)
        hasHorror = not . null . (^.. key "genres" . _Array . folded . filtered isHorror)

withStatus :: ByteString -> Maybe String
withStatus = (^? key "status" . _String) >=> (Just . T.unpack)

parseMovies :: ByteString -> [Movie]
parseMovies = mapMaybe makeMovie . (^.. key "data" . key "movies" . _Array . traverse)

list :: IO [Movie]
list = do
  response <- (^? responseBody) <$> getWith opts "https://yts.to/api/v2/list_movies.json"
  case response >>= withStatus of
    Just "ok" -> return $ maybe [] parseMovies response
    status    -> fail $ show status
  where opts = defaults & param "limit" .~ ["50"]
                        & param "quality" .~ ["1080p"]
                        & param "minimum_rating" .~ ["4"]
                        & param "sort_by" .~ ["date_added"]
