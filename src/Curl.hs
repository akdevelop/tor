module Curl
( getFile
) where

import           System.Directory
import           System.Process

getFile :: FilePath -> String -> IO ()
getFile dir url = do
  currentDir <- getCurrentDirectory
  setCurrentDirectory dir
  callCommand $ "curl --socks5 localhost:1080 -OJLs " ++ url
  setCurrentDirectory currentDir
