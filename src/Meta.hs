{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Meta
( Movie(..)
, parse
, stringify
, fetchMovieIDByTitle
, fetchMovieByTitle
, addToMeta
) where

import           Control.Lens
import           Control.Monad
import           Control.Monad.Trans.Maybe
import qualified Curl
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.ByteString.Lazy      (ByteString)
import qualified Data.ByteString.Lazy      as B
import           Data.List
import           Data.Maybe
import qualified Data.Text                 as T
import           GHC.Generics
import           Network.Wreq
import           System.Directory
import           System.FilePath.Posix
import           System.IO
import qualified Titles

data Movie = Movie
  { title  :: String
  , year   :: Integer
  , rating :: Double
  , rtC    :: Integer
  , rtA    :: Integer
  , genres :: [String]
  , intro  :: String
  , descr  :: String
  , cover  :: Maybe String
  , shot1  :: Maybe String
  , shot2  :: Maybe String
  , shot3  :: Maybe String
  } deriving (Show, Generic)

instance FromJSON Movie
instance ToJSON Movie

parse :: ByteString -> [Movie]
parse jsonText =
  case decode jsonText of
    Just movies -> movies
    _           -> error "bad Movie file"

stringify :: [Movie] -> ByteString
stringify = encode . map (\movie ->
  movie { cover = Nothing
        , shot1 = Nothing
        , shot2 = Nothing
        , shot3 = Nothing })

withStatus :: ByteString -> Maybe String
withStatus = (^? key "status" . _String) >=> (Just . T.unpack)

parseMovie :: ByteString -> Maybe Movie
parseMovie r = do
    movie <- r ^? key "data"
    dTitle <- movie ^? key "title_long" . _String >>= strUnpack
    dYear <- movie ^? key "year" . _Integer
    dRating <- movie ^? key "rating" . _Double
    dRtC <- movie ^? key "rt_critics_score" . _Integer
    dRtA <- movie ^? key "rt_audience_score" . _Integer
    dGenres <- movie ^? key "genres" . _Array >>= listUnpack
    dIntro <- movie ^? key "description_intro" . _String >>= strUnpack
    dDescr <- movie ^? key "description_full" . _String >>= strUnpack
    dCover <- movie ^? key "images"
                    . key "medium_cover_image" . _String >>= strUnpack
    dShot1 <- movie ^? key "images"
                    . key "medium_screenshot_image1" . _String >>= strUnpack
    dShot2 <- movie ^? key "images"
                    . key "medium_screenshot_image2" . _String >>= strUnpack
    dShot3 <- movie ^? key "images"
                    . key "medium_screenshot_image3" . _String >>= strUnpack
    return $ Movie dTitle dYear dRating dRtC dRtA dGenres dIntro dDescr
      (Just dCover) (Just dShot1) (Just dShot2) (Just dShot3)
  where strUnpack = Just . T.unpack
        listUnpack = Just . map T.unpack . (^.. traverse . _String)

fetchMovieByID :: Integer -> IO (Maybe Movie)
fetchMovieByID movieId = do
    body <- (^? responseBody) <$> getWith opts "https://yts.to/api/v2/movie_details.json"
    case body >>= withStatus of
      Just "ok" -> return (body >>= parseMovie)
      status    -> fail $ "movie_details => " ++ show status
  where opts = defaults & param "movie_id" .~ [T.pack $ show movieId]
                        & param "with_images" .~ ["true"]

getMovieID :: String -> ByteString -> Maybe Integer
getMovieID t body = do
    movies <- body ^? key "data" . key "movies" . _Array
    movie <- find exactMatch (movies ^.. traverse)
    movie ^? key "id" . _Integer
  where title_long = (^? key "title_long" . _String) >=> (Just . T.unpack)
        exactMatch = maybe False ((== Titles.sanitize t) . Titles.sanitize) . title_long

fetchMovieIDByTitle :: String -> IO (Maybe Integer)
fetchMovieIDByTitle t = do
    let searchString = searchFriendly t
        opts = defaults & param "query_term" .~ [T.pack searchString]
                        & param "quality" .~ ["1080p"]
                        & param "sort_by" .~ ["date_added"]
    body <- (^? responseBody) <$> getWith opts "https://yts.to/api/v2/list_movies.json"
    case body >>= withStatus of
      Just "ok" -> return (body >>= getMovieID t)
      status    -> fail $ "list_movies => " ++ show status
  where searchFriendly = unwords . take 4 . words . Titles.stripYear

fetchMovieByTitle :: String -> IO (Maybe Movie)
fetchMovieByTitle = runMaybeT . ((MaybeT . fetchMovieIDByTitle) >=> (MaybeT . fetchMovieByID))

copyImages :: FilePath -> Movie -> IO ()
copyImages dir movie = do
    let out = dir </> title movie
    createDirectoryIfMissing True out
    value (Curl.getFile out) (cover movie)
    value (Curl.getFile out) (shot1 movie)
    value (Curl.getFile out) (shot2 movie)
    value (Curl.getFile out) (shot3 movie)
  where value = maybe (return ())

overwrite :: FilePath -> ByteString -> IO ()
overwrite filename text = do
  tmpdir <- getTemporaryDirectory
  (tmpFile, tmpHandle) <- openTempFile tmpdir filename
  B.hPut tmpHandle text
  hClose tmpHandle
  renameFile tmpFile filename

addToMeta :: FilePath -> FilePath -> [Integer] -> IO ()
addToMeta descrFile imagesDir movieIDs = do
  movieList <- parse <$> B.readFile descrFile
  foldM_ (\movies uid -> do
      movie <- fromJust <$> fetchMovieByID uid
      copyImages imagesDir movie
      let newMovieList = movie : movies
      overwrite descrFile (stringify newMovieList)
      return newMovieList
    ) movieList movieIDs
