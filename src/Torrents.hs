module Torrents
( Movie(..)
, download
) where

import           Control.Monad
import qualified Curl
import           Data.Char
import           System.IO
import           System.IO.Temp
import           System.Process

class Movie a where
  title :: a -> String
  url :: a -> String

confirm :: (Movie a) => [a] -> IO Bool
confirm movies = do
    mapM_ (putStrLn . ("* " ++) . title) movies
    putStr "Download? [Y/N] "
    hFlush stdout
    isYes <$> getLine
  where isYes = (== 'y') . toLower . head . head . lines

download :: (Movie m) => [m] -> IO ()
download movies = do
  confirmed <- confirm movies
  when confirmed $
    withSystemTempDirectory "torrents." $ \tmpdir -> do
      forM_ movies $ \movie ->
        Curl.getFile tmpdir (url movie)
      callCommand $ "scp -r " ++ tmpdir ++ "/* odroid:~/torrents"
