module Titles
( fromString
, stripYear
, sanitize
) where

import           Data.Char
import           Data.List
import qualified Data.Map.Strict as Map

drop1080p :: String -> String
drop1080p line
  | " 1080p" `isSuffixOf` line   = drop1080p . reverse . drop 6 . reverse $ line
  | " [1080p]" `isSuffixOf` line = drop1080p . reverse . drop 8 . reverse $ line
  | otherwise                    = line

noAccents :: String -> String
noAccents str =
  let table = Map.fromList
        [ ('\xC1', 'A')
        , ('\xE1', 'a')
        , ('\xC9', 'E')
        , ('\xE9', 'e')
        , ('\xCD', 'I')
        , ('\xED', 'i')
        , ('\xD3', 'O')
        , ('\xF3', 'o')
        , ('\xDA', 'U')
        , ('\xFA', 'u')
        , ('\xFC', 'u')
        , ('\xD1', 'N')
        , ('\xF1', 'n')
        , ('\xC7', 'C')
        , ('\xE7', 'c') ]
  in map (\c -> Map.findWithDefault c c table) (filter (not . isMark) str)

stripYear :: String -> String
stripYear line =
    case reverse line of
      ')':n1:n2:n3:n4:'(':' ':xs
        | isYear n1 n2 n3 n4 -> stripYear . reverse $ xs
        | otherwise          -> line
      _                      -> line
  where isYear n1 n2 n3 n4 = all isDigit [n1, n2, n3, n4]

sanitize :: String -> String
sanitize = map toLower . noAccents .
  unwords . words . concatMap (
    (\w -> if w == "&" then " and " else w) . (:"") .
    (\c -> if c /= '&' && isPunctuation c then ' ' else c)
  ) . stripYear

fromString :: String -> [String]
fromString = nub . map drop1080p . lines
