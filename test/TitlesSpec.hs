module TitlesSpec ( spec ) where

import           Data.Char
import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import qualified Titles

spec :: Spec
spec = do
  it "stripYear" $
    forAll (vectorOf 4 (suchThat arbitrary isDigit)) $ \ns ->
      and [
        ns == Titles.stripYear (ns ++ " (" ++ ns ++ ")"),
        ns /= Titles.stripYear (ns ++ " (test)")
      ]
  it "sanitize" $
    forAll (listOf1 (suchThat arbitrary (not . isControl))) $ \str ->
      not . any isPunctuation $ Titles.sanitize str
  it "fromString" $
    forAll (listOf1 (suchThat arbitrary (/= '\n'))) $ \str ->
      let cases = [str, str ++ " 1080p", str ++ " [1080p]"]
      in all (not . isSuffixOf " 1080p") (map (head . Titles.fromString) cases)
